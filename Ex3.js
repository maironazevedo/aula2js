let sortedGods = gods.sort((god1, god2) => {
    return god1.pantheon.localeCompare(god2.pantheon)
})

console.log(sortedGods)